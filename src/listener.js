'use strict';

const config = require('config');
const util = require('util');
const note = require('note-log');

const irc = require('irc');
const client = new irc.Client(config.get('host'), config.get('nick'), config.get('options'));

const router = require('./router.js');

module.exports = function() {
    client.addListener('join', (channel, nick) => {
        if(nick === config.get('nick')) {
            note('listen', 0, 'Joined ' + channel);
        }
    });

    client.addListener('selfMessage', (to, message) => {
        note('listen', to + ' <' + config.get('nick') + '> ' + message);
    });

    client.addListener('message', (from, to, message) => {
        const nick = message.match(config.get('nick'));
        const prefixed = message.match(new RegExp('^' + config.get('prefix') + '\\w+'));
        const code = message.match(new RegExp('^\\w+' + config.get('code')));

        if(to === config.get('nick')) {
            to = from;
        }

        Promise.resolve().then(() => {
            if(nick) {
                note('listen', to + ' <' + from + '> ' + message);

                return router('nick', null, from, to);
            }

            if(prefixed) {
                note('listen', to + ' <' + from + '> ' + message);

                const command = prefixed[0].match(/\w+/)[0];
                const arg = message.slice(prefixed[0].length).trim();

                return router(command, arg, from, to);
            }

            if(code) {
                note('listen', to + ' <' + from + '> ' + message);

                const language = code[0].match(/\w+/)[0];
                const arg = message.slice(code[0].length).trim();

                return router(language, arg, from, to);
            }
        }).then(results => {
            if(results) {
                results = results.map(result => {
                    return result.replace(/\n|\r/g, '');
                }).join(' ').slice(0, config.get('replyLimit'));

                client.say(to, results);
            }
        }).catch(error => {
            note('listen', error);
        });
    });

    client.addListener('error', message => {
        note('listen', 2, util.inspect(message));
    });
};
