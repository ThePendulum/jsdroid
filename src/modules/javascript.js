'use strict';

const config = require('config');
const note = require('note-log');

const Writable = require('stream').Writable;

const Docker = require('dockerode');
const docker = new Docker();

module.exports = function(arg, user, channel) {
    const ws = Writable();
    const results = [];

    return new Promise((resolve, reject) => {
        ws._write = function(chunk, enc, next) {
            const result = chunk.toString().replace(/\r/g, '');

            results.push(chunk.toString());

            next();
        };

        ws.on('finish', () => {
            resolve(results);
        });

        docker.run(config.get('images.javascript'), ['node', '--harmony_destructuring', '--harmony_array_includes', '--harmony_rest_parameters', '--harmony', '-p', '-e', arg], ws, (error, container) => {
            if(error) {
                return reject(error);
            }
        });
    });
};
