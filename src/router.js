'use strict';

const config = require('config');

const modules = {
    nick: require('./modules/nick.js'),
    js: require('./modules/javascript.js'),
    php: require('./modules/php.js'),
    py: require('./modules/python.js')
};

module.exports = function(command, arg, user, channel) {
    return modules[command](arg, user, channel);
};
