'use strict';

module.exports = {
    host: 'irc.freenode.net',
    nick: 'jsdroid',
    options: {
        port: 6697,
        secure: true,
        userName: 'jsdroid',
        realName: 'JS Droid',
        channels: ['#web-sandbox']
    },
    prefix: '~',
    code: '>',
    replyLimit: 200,
    images: {
        javascript: 'node:5.10.1-slim',
        php: 'php:7.0.5-alpine',
        python: 'python:3.5.1-alpine'
    }
};
